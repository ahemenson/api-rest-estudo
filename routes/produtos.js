const express = require('express');
const router = express.Router();

// Retorna todos os produtos
router.get('/', (req, res, next) => {
  res.status(200).send({
    mensagem: 'Retorna todos os produtos'
  });
});

// Insere um produto
router.post('/', (req, res, next) => {
  const produto = {
    nomeProduto: req.body.nome,
    precoProduto: req.body.preco
  };
  res.status(201).send({
    mensagem: 'O produto foi criado',
    produtoCriado: produto
  });
});

// Retorna os dados de um produto
router.get('/:id_produto', (req, res, next) =>{
  const id = req.params.id_produto;

  if(id === 'especial') {
    res.status(200).send({
      mensagem: 'Usando o Get de um produto especial',
      id: id
    });

  } else {
    res.status(200).send({
      mensagem: 'Usando o Get de um produto exclusivo',
      id: id
    });
  }

});

// Altera um produto
router.patch('/', (req, res, next) => {
  res.status(201).send({
    mensagem: 'Usando o PATCH dentro da rota de produtos'
  });
});

// Exclui um produto
router.delete('/', (req, res, next) => {
  res.status(201).send({
    mensagem: 'Usando o DELETE dentro da rota de produtos'
  });
});

module.exports = router;
